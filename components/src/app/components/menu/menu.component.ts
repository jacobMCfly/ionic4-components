import { Component, OnInit } from '@angular/core';
import { Componente } from '../../interfaces/menu';
import { Observable } from 'rxjs';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  componentes: Array<Componente>;

  constructor(private dataServiceMenu: DataService) { }

  ngOnInit() {

    this.dataServiceMenu.getOptions().subscribe((data) => {
      this.componentes = data;
      console.log(this.componentes);
    });


  }

}
