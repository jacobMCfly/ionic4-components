import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popinfo',
  templateUrl: './popinfo.component.html',
  styleUrls: ['./popinfo.component.scss'],
})
export class PopinfoComponent implements OnInit {

  @Input() arrayList;

  constructor(private popoverCtrl: PopoverController) { }

  ngOnInit() {
    console.log(this.arrayList);
  }

  getData(item: any) {
    console.log(item);
    this.popoverCtrl.dismiss({
      returnedItem: item
    });
  }

}
