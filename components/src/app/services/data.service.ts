import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Componente } from '../interfaces/menu';
import { Subject, Observable } from 'rxjs';
import { Albums } from '../interfaces/albums';
import { PML } from '../interfaces/pml';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { environment } from '../../environments/environment';
import { SuperHeroe } from '../interfaces/superheroes';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {




  constructor(private http: HttpClient, private firestore: AngularFirestore) { }

  // async getOptions() {
  //   try {
  //     const result = await this.http.get<Componente[]>('/assets/data/primerMenu.json').toPromise();
  //     return result;
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  getOptions() {
    const result = this.http.get<Componente[]>('/assets/data/primerMenu.json');
    return result;
  }

  getAlbums() {
    const albums = this.http.get<Albums[]>('http://jsonplaceholder.typicode.com/albums');
    return albums;
  }

  getSuperHeroes() {
    const spHeroes = this.http.get<SuperHeroe[]>('/assets/data/superheroes.json').pipe(delay(1000));
    return spHeroes;
  }

  getPML(itemSE, itemTP, itemNL, initialYear, finalYear) {
    const header = new HttpHeaders();
    header.append('Access-Control-Allow-Origin', '*');
    header.append('Content-Tyoe', 'application/+json');
    // header.append('Content-Type', 'application/x-www-form-urlencoded');

    const composeUrl = 'https://ws01.cenace.gob.mx:8082/SWPEND/SIM/';
    const params = itemSE + '/' + itemTP + '/' + itemNL + '/' + initialYear + '/' + finalYear;
    const url = composeUrl + params + '/' + 'JSON';
    console.log(url);
    const pml = this.http.get<PML[]>(url, { headers: header });
    return pml;
  }

  findFireBaseUser() {
    const findUser = this.firestore.collection('usuarios', ref => ref.where('username', '==', 'joaquinlugo')).valueChanges();
    return findUser;
  }

  setFireBaseUser() {
    const set = this.firestore.collection('usuarios').doc('uno').set({
      username: 'bar',
      password: 'x'
    });
    return set;
  }
  getFireBase() {
    const data = this.firestore.collection('usuarios').valueChanges();
    return data;
  }

}
