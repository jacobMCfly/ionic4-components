import { Component, OnInit } from '@angular/core';
import { ModalInfoPage } from '../modal-info/modal-info.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor(public modalCtr: ModalController) { }

  ngOnInit() {
  }

  async abrirModal() {

    const myModal = await this.modalCtr.create({
      component: ModalInfoPage,
      componentProps: {
        nombre: 'joaquin',
        pais: 'mexico'
      }
    });
    await myModal.present();

    const { data } = await myModal.onDidDismiss();
    console.log('retorno del modal', data);


  }

}
