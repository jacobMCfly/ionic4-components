import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { DataService } from '../../services/data.service';
import { PML } from '../../interfaces/pml';

@Component({
  selector: 'app-sw-pend',
  templateUrl: './sw-pend.page.html',
  styleUrls: ['./sw-pend.page.scss'],
})
export class SWPENDPage implements OnInit {
  pml: PML[];
  initialYear: any;
  finalYear: any;
  electricSistem = ['SIN', 'BCA', 'BCS'];
  proceso = ['MDA', 'MTR'];
  listaNodos = [
    'Acapulco'
  ];
  itemSE: any;
  itemTP: any;
  itemNL: any;
  constructor(private dataService: DataService) {
    // tslint:disable-next-line: prefer-const
    let getDate = new Date();
    const yearInit = getDate.getFullYear();
    const monthInit = getDate.getMonth();
    const dayInit = getDate.getDay();
    const composeDate = yearInit + '-' + monthInit + '-' + dayInit;
    this.initialYear = composeDate;
    this.finalYear = composeDate;
    console.log(this.initialYear);
  }

  ngOnInit() {

  }

  electricSistemChange(e) {
    console.log(e.target.value);
    this.itemSE = e.target.value;
  }
  processChange(e) {
    console.log(e.target.value);
    this.itemTP = e.target.value;
  }
  nodeChange(e) {
    console.log(e.target.value);
    this.itemNL = e.target.value;
  }

  Remove(str, startIndex, count) {
    return str.substr(0, startIndex) + str.substr(startIndex + count);
  }

  Enviar() {
    const dateinitRemoveTime = this.Remove(this.initialYear, 10, this.initialYear.length);
    const dateFinalRemoveTime = this.Remove(this.finalYear, 10, this.initialYear.length);
    console.log();
    console.log(this.finalYear);
    if (this.itemNL) {
      const joinNodes = this.itemNL.join(',');
      const deleteSpace = joinNodes.replace(/\s/g, '');
      const toUpperCase = deleteSpace.toUpperCase();
      const prepareInitialYear = dateinitRemoveTime.replace(/-/g, '/');
      const prepareFinalYear = dateFinalRemoveTime.replace(/-/g, '/');
      console.log(prepareInitialYear);
      console.log(prepareFinalYear);

      this.dataService.getPML(this.itemSE, this.itemTP, toUpperCase, prepareInitialYear, prepareFinalYear).subscribe((data) => {
        this.pml = data;
        console.log(this.pml);
      });
    }
  }

}
