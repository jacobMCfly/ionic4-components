import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Albums } from '../../interfaces/albums';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.page.html',
  styleUrls: ['./searchbar.page.scss'],
})
export class SearchbarPage implements OnInit {

  albums: Albums[];
  searchData = '';
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getAlbums().subscribe((data) => {
      this.albums = data;
      console.log(this.albums);
    });
  }


  onSearchChange(event) {
    this.searchData = event.detail.value;
  }
}
