import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Componente } from '../../interfaces/menu';
import { Observable } from 'rxjs';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Array<Componente>;
  componentes2: Observable<Componente[]>;

  constructor(private menuCtrl: MenuController, private dataService: DataService) { }
  ngOnInit() {
    // this.getComponentes();
    this.dataService.getOptions().subscribe((data) => {
      console.log(data);
      this.componentes = data;
    });
    this.dataService.setFireBaseUser();
    this.dataService.findFireBaseUser().subscribe(data => {
      console.log(data);
    });
    this.dataService.getFireBase().subscribe(data => {
      console.log(data);
    });
  }

  // async getComponentes() {
  //   this.componentes = await this.dataService.getOptions();
  // }



}
