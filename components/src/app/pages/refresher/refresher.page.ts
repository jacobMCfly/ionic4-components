import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refresher',
  templateUrl: './refresher.page.html',
  styleUrls: ['./refresher.page.scss'],
})
export class RefresherPage implements OnInit {
  item: any[] = [];
  constructor() { }

  ngOnInit() {
  }

  doRefresh(event) {
    setTimeout(() => {
      this.item = Array(40);
      console.log(this.item);
      event.target.complete();
    }, 1500);
  }

}
