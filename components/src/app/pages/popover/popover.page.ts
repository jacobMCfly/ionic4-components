import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopinfoComponent } from '../../components/popinfo/popinfo.component';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  async mostrarPop($event: any) {

    const dataList = ['1', '2', 'dato3', 'dato4'];

    const popover = await this.popoverCtrl.create({
      component: PopinfoComponent,
      event: $event,
      translucent: true,
      mode: 'ios',
      backdropDismiss: false,
      componentProps: {
        arrayList: dataList
      }
    });

    await popover.present();

    const { data } = await popover.onDidDismiss();
    console.log('returned data', data);

  }

}
