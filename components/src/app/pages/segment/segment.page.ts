import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { SuperHeroe } from '../../interfaces/superheroes';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  @ViewChild('segmento', { static: true }) segment: IonSegment;  // elemento del view
  superHeroes: SuperHeroe[]; // elemento para el get de la interface
  filtroViewData = '';

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getSuperHeroes().subscribe((data) => {
      this.superHeroes = data;
      console.log(data);
    });
    this.segment.value = 'todos';
  }
  segmentChanged($event) {
    const segmentValue = $event.detail.value;
    console.log(segmentValue);
    if (segmentValue === 'todos') {
      this.filtroViewData = '';
      return;
    }
    this.filtroViewData = segmentValue;
  }
}
