import { Component, OnInit, enableProdMode } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-input',
  templateUrl: './input.page.html',
  styleUrls: ['./input.page.scss'],
})
export class InputPage implements OnInit {
  nombre: string;
  loginForm: FormGroup;
  loginFormValidate: any = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: ['']
    });
    this.loginFormValidate = [
      { emailAlert: 'Favor de introducir un email valido', visible: false },
      { passwordAlert: 'Contraseña inválida', visible: false }
    ];
    this.loginForm.valueChanges.subscribe(check => {
      this.loginForm.controls.email.valid === false ? this.loginFormValidate[0].visible = true : this.loginFormValidate[0].visible = false;
      if (this.loginForm.controls.email.value === '') { this.loginFormValidate[0].visible = false; }
      // tslint:disable-next-line: max-line-length 
      this.loginForm.controls.password.valid === false ? this.loginFormValidate[1].visible = true : this.loginFormValidate[1].visible = false;
      console.log(this.loginFormValidate[1].visible); 
    });
  }

  onSubmitLogin() {
    console.log(this.loginForm.controls.email.valid);
    console.log('"submitted"');
  }

}
