export interface PML {
    nombre: string;
    proceso: string;
    sistema: string;
    area: string;
    // tslint:disable-next-line: ban-types
    Resultados: Array<any>;
}