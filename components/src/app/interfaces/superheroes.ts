// tslint:disable-next-line: class-name
export interface SuperHeroe {
    superhero: string;
    publisher: string;
    alter_ego: string;
    first_appearance: string;
    characters: string;
}
