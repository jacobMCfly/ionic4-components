// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA9W2CoqzEuN7diix_OplkXjyWXR4Q8RPs',
    authDomain: 'energy-nodep.firebaseapp.com',
    databaseURL: 'https://energy-nodep.firebaseio.com',
    projectId: 'energy-nodep',
    storageBucket: 'energy-nodep.appspot.com',
    messagingSenderId: '196684885071',
    appId: '1:196684885071:web:c67b0a1dea909828ea8e3a',
    measurementId: 'G-RLQ4SNV4XD'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
